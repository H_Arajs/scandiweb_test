<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">

    <title>Product add</title>

    <!-- checks for empty fields in form -->
    <script>
    function checkForBlank() {
    if (document.getElementById("SKU_box").value == "" || document.getElementById("name_box").value == "" || document.getElementById("price_box").value == "") {
        alert("Type out all fields");
        return false;
    }



};
    
    </script>

  </head>
  <body>


  <section class="top">
    <div class="container">
    <div class="row mt-5">
    <div class="col-6">
    <form action="insert.php" method="POST" onsubmit="return checkForBlank()">
   
    <h1>Product add</h1>
    
    </div>
    <div class="col-6 text-right">
    <button type="submit" class="btn btn-success">Add product</button>
    <br>
    <a href="product_list.php"><button type="button" class="btn btn-secondary mt-5 nav_button">Go to Product List page</button></a>
    
    </div>
    
    
    </div>
    
    
    </div>
  
  </section>


    <section class="main mt-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="d-flex justify-content-start">
                          <p class="product_text">SKU</p>
                        <input type="text" id="SKU_box" class="input_box" name="SKU"></input>

                    </div>
                    <div class="d-flex justify-content-start">
                        <p class="product_text">Name</p>
                        <input type="text" id="name_box" class="input_box input_box_name" name="Title"></input>

                    </div>
                    <div class="d-flex justify-content-start">
                        <p class="product_text">Price</p>
                        <input type="text" id="price_box" class="input_box input_box_price" name="Price"></input>

                    </div>

                    <!-- type switcher -->
                    <div class="d-flex justify-content-start mt-3">
                        <p class="product_text">Choose type</p>
                        <button type="button" class="btn btn-primary ml-4 dvd_type">DVD-disc</button>
                        <button type="button" class="btn btn-primary ml-4 book_type">Book</button>
                        <button type="button" class="btn btn-primary ml-4 furniture_type">Furniture</button>
                    

                    </div>
                    
                    <!-- disc option -->
                    <div class="d-flex justify-content-start mt-4 item_type_dvd hide">
                    <p class="product_text">Size</p>
                        <input type="text" id="size_box" class="ml-3 input_box" placeholder="Provide size in Megabytes" name="Size"></input>



                    </div>

                    <!-- furniture option -->
                    <div class="d-flex justify-content-start mt-3 item_type_furniture hide">
                        <p class="product_text">Height</p>
                        <input type="text" id="height_box" class="ml-3 input_box" placeholder="Provide size in centimetres" name="Height"></input>
                        <p class="product_text ml-5">Width</p>
                        <input type="text" id="width_box" class="ml-3 input_box" placeholder="Provide size in centimetres" name="Width"></input>
                        <p class="product_text ml-5">Length</p>
                        <input type="text" id="length_box" class="ml-3 input_box" placeholder="Provide size in centimetres" name="Length"></input>
                


                    </div>

                    <!-- book option -->
                    <div class="d-flex justify-content-start item_type_book hide">
                    <p class="product_text">Weight</p>
                        <input type="text" id="weight_box" class="ml-3 input_box" placeholder="Provide size in kilograms" name="Weight"></input>


                    </div>



                </div>



            </div>

        </div>


    </section>

    </form>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="js/script.js"></script>
  </body>
</html>


<!-- <select id="dropdown" required class="mt-3">
                    <option disabled selected value>Select product type</option>
                    <option id="dvd_type" >DVD-disc</option>
                    <option >Book</option>
                    <option >Furniture</option>
            
            
                     </select> -->