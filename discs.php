<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "scandiweb";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM discs";
$discs = $conn->query($sql);

if ($discs->num_rows > 0) {
  // output data of each row
  while($row = $discs->fetch_assoc()) {
    echo "<div class='col-md-3 col-sm-6 col-6 mt-2'>" . "<div class='item text-center align-middle'>" . "<input type='checkbox'>" . "<h4 class='mt-5'>" . $row["SKU"]. "</h4>" . "<h5>" . $row["Title"] . "</h5>" . "<h5>" . $row["Price"] . "</h5>" . "<h5>" . $row["Size"] . "</h5>" . "<br>" . "</div>" . "</div>";
  }
} else {
  echo "0 results";
}
$conn->close();
?>