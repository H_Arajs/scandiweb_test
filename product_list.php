

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">

    <title>Product list</title>



  </head>
  <body>



  <section class="top">
    <div class="container">
    <div class="row mt-5">
    <div class="col-6">
    <h1>Product list</h1>
    
    </div>
    <div class="col-6 text-right">
    <button type="button" id="delete_button" class="btn btn-danger mt-2">Mass Delete</button>
    <br>
    <a href="product_add.php"><button type="button" class="btn btn-secondary mt-5 ml-5">Go to Product Add page</button></a>
    
    </div>
    
    
    </div>
    
    
    </div>
  
  </section>



  <section class="items mt-5">
    <div class="container">
    <div class="row">

    <!-- retrieves data from database "disc" -->
    <?php include "discs.php"


    ?>

    <!-- retrieves data from database "books" -->
    <?php include "books.php"


    ?>

    <!-- retrieves data from database "furniture" -->
    <?php include "furniture.php"


    ?>
   

 
    
    
    </div>
    
    </div>
  
  
  </section>





    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="js/script.js"></script>
  </body>
</html>