"use strict;"


$(document).ready(function(){
    $(".dvd_type").on("click", function() {
        $(".item_type_dvd").removeClass("hide");
        $(".item_type_book").addClass("hide");
        $(".item_type_furniture").addClass("hide");
    });

})

$(document).ready(function(){
    $(".book_type").on("click", function() {
        $(".item_type_book").removeClass("hide");
        $(".item_type_dvd").addClass("hide");
        $(".item_type_furniture").addClass("hide");
        
    });

})

$(document).ready(function(){
    $(".furniture_type").on("click", function() {
        $(".item_type_furniture").removeClass("hide");
        $(".item_type_dvd").addClass("hide");
        $(".item_type_book").addClass("hide");
    });

})

$(document).ready(function() {
    $("#delete_button").on("click",function(){
        $("input:checkbox").each(function() {
            if ($(this).is(":checked")) {
                $(this).parent().remove();
            }
        });
    })})