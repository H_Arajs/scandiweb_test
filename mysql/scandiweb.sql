-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 10, 2020 at 11:56 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scandiweb`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `SKU` varchar(255) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Price` varchar(255) NOT NULL,
  `Weight` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`SKU`, `Title`, `Price`, `Weight`) VALUES
('GGWP007', 'War and Peace', '20.00 $\r\n', '2 KG'),
('EHM891', 'The Old Man and The Sea', '15.00 $', '1 KG'),
('CBW0091', 'Hollywood', '10.00$', '0.7 KG'),
('PTSM981', 'Kids', '25.00 $\r\n', '3 KG');

-- --------------------------------------------------------

--
-- Table structure for table `discs`
--

CREATE TABLE `discs` (
  `SKU` varchar(255) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Price` varchar(255) NOT NULL,
  `Size` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `discs`
--

INSERT INTO `discs` (`SKU`, `Title`, `Price`, `Size`) VALUES
('JVC20012', 'Acme Disc', '1.00 $', '700 MB'),
('GBH20142', 'Blank Disc', '1.50 $ ', '700 MB'),
('GST34512', 'Blank Disc #2', '5.00', '1400 MB'),
('MTL5004', 'Acme Disc #2', '3.00 $', '4700 MB');

-- --------------------------------------------------------

--
-- Table structure for table `furniture`
--

CREATE TABLE `furniture` (
  `SKU` varchar(255) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Price` varchar(255) NOT NULL,
  `Height` varchar(255) NOT NULL,
  `Width` varchar(255) NOT NULL,
  `Length` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `furniture`
--

INSERT INTO `furniture` (`SKU`, `Title`, `Price`, `Height`, `Width`, `Length`) VALUES
('TR120555', 'Chair', '40.00 $', '24', '45', '15'),
('IKR5601', 'Table', '65.00$', '40', '40', '40'),
('CH134343', 'Couch', '250 $', '90', '100', '150'),
('KMGF5445', 'Bookshelf', '80.00 $', '180', '70', '50');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `SKU` varchar(255) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Price` varchar(255) NOT NULL,
  `Size` varchar(255) DEFAULT NULL,
  `Height` varchar(255) DEFAULT NULL,
  `Width` varchar(255) DEFAULT NULL,
  `Length` varchar(255) DEFAULT NULL,
  `Weight` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`SKU`, `Title`, `Price`, `Size`, `Height`, `Width`, `Length`, `Weight`, `id`) VALUES
('JVC200123', 'Acme Disc', '1.00 $', '700 MB', '', '', '', '', 32),
('GBH20142', 'Blank Disc', '1.50 $', '700 MB', '', '', '', '', 33),
('GST34512', 'Blank Disc #2', '5.00 $', '700 MB', '', '', '', '', 34),
('MTL5004', 'Acme Disc #2', '3.00 $', '4700 MB', '', '', '', '', 35),
('GGWP007', 'War and Peace', '20.00 $', '', '', '', '', '2 KG', 36),
('EHM891', 'The Old Man and The Sea', '15.00 $', '', '', '', '', '1 KG', 37),
('CBW0091', 'Hollywood', '10.00 $', '', '', '', '', '0.7 KG', 38),
('PTSM981', 'Kids', '25.00', '', '', '', '', '3 KG', 39),
('TR120555', 'Chair', '40.00 $', '', '24', '45', '15', '', 40),
('IKR5601', 'Table', '65.00 $', '', '40', '40', '40', '', 41),
('CH1343434', 'Couch', '250.00 $', '', '90', '100', '150', '', 42),
('KMGF5445', 'Bookshelf', '80.00 $', '', '180', '70', '50', '', 43);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
